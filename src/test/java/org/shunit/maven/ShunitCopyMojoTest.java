package org.shunit.maven;

import java.io.File;

import org.apache.maven.plugin.testing.AbstractMojoTestCase;
import org.shunit.maven.mojo.ShunitResourcesMojo;

public class ShunitCopyMojoTest extends AbstractMojoTestCase {
	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		// required for mojo lookups to work
		super.setUp();
	}
	
	/** {@inheritDoc} */
    protected void tearDown()
        throws Exception
    {
        // required
        super.tearDown();
    }

	/**
	 * @throws Exception
	 */
	public void testMojoGoal() throws Exception {
	
		File testPom = new File(getBasedir(),
				"src/test/resources/unit/basic-test/sample-sh-project-config.xml");

		ShunitResourcesMojo mojo = (ShunitResourcesMojo) lookupMojo("prepare",testPom);
		
		assertNotNull(mojo);
		
		mojo.execute();		
	}
}
