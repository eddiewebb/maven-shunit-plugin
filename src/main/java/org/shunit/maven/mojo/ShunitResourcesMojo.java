/**
 * Copyright 2013 - Edward A. Webb
 * maven-shunit-plugin - 
 *
 */
package org.shunit.maven.mojo;

import java.io.File;

import org.apache.commons.lang3.Validate;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

/**
 * Responsible for copying shUnit tests to the target directory during generate-resources phase.
 * Optionallly provides filtering.
 * @author Edward Webb
 *
 */
@Mojo( name = "prepare",defaultPhase=LifecyclePhase.GENERATE_RESOURCES,requiresProject=true )
public class ShunitResourcesMojo extends AbstractMojo {

	/**
	  * Project instance
	  * 
	  * @parameter default-value="${project}"
	  * @required
	  * @readonly
	  */
	private MavenProject project;
	
	/**
	  * Project base directory
	  * 
	  * @parameter default-value="${basedir}"
	  * @required
	  * @readonly
	  */
	private File projectBaseDir;
	
    @Parameter(  defaultValue = "src/test/script" )    
    private File testDirectory;

    @Parameter(  defaultValue = "shunit2_test_" )
    private String testPrefix;
    
    
	/* (non-Javadoc)
	 * @see org.apache.maven.plugin.Mojo#execute()
	 */
	public void execute() throws MojoExecutionException, MojoFailureException {
		Validate.notNull(projectBaseDir);
		Validate.notNull(project);
		if ( !testDirectory.isAbsolute() )
		{
			testDirectory = new File( projectBaseDir, testDirectory.getPath() );
		}
		// TODO Auto-generated method stub
		getLog().info("Copying shUnit tests from " + testDirectory.getAbsolutePath() + " with naming " + testPrefix + "*");
	}

}
