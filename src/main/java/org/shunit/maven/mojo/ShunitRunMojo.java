/**
 * Copyright 2013 - Edward A. Webb
 * maven-shunit-plugin - 
 *
 */
package org.shunit.maven.mojo;

import java.io.File;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

/**
 * Responsible for copying shUnit tests to the target directory during generate-resources phase.
 * Optionallly provides filtering.
 * @author Edward Webb
 *
 */
@Mojo( name = "run",defaultPhase=LifecyclePhase.TEST,requiresProject=true )
public class ShunitRunMojo extends AbstractMojo {

	/**
	  * Project instance
	  * 
	  * @parameter default-value="${project}"
	  * @required
	  * @readonly
	  */
	private MavenProject project;
	
   

    @Parameter(  defaultValue = "shunit2_runner.sh" )
    private String testRunner;
    
    
	/* (non-Javadoc)
	 * @see org.apache.maven.plugin.Mojo#execute()
	 */
	public void execute() throws MojoExecutionException, MojoFailureException {
		
		// TODO Auto-generated method stub
		getLog().info("Executing shUnit Runner" + testRunner);
	}

}
